const margin2 = 50;
const width2 = 1000 - 2 * margin2;
const height2 = 600 - 2 * margin2;

const svg2 = d3.select('#svg2');
var table = [];
var moyenne = [];

var i = 0;

data2.forEach(function(element) {
  if (!typeof element['fields']['c2cha4'] !== 'undefined') {
    
    var date = new Date(element['fields']['dateheure']);
    var hour = date.getHours();

    if (!table[hour]) {
      table[hour] = 0;
    }
    table[hour] += element['fields']['c2cha4'];
    if (!moyenne[hour]) {
      moyenne[hour] = 0;
    }
    moyenne[hour]++;
  }
});

const sample2 = [];

for (let i = 0; i < 24; i++) {
  sample2.push({ y: parseInt(table[i] / moyenne[i] * 100), x: i});
}

var n = 24;

// AXES
const chart2 = svg2
.append('g')
.attr('transform', `translate(${margin2}, ${margin2})`);

// scale will use the index of our data
const yScale2 = d3
.scaleLinear()
.range([height2, 0])
.domain([0, 800]);

chart2
.append('g')
.attr('stroke', '#FFF')
.attr('class', 'axe')
.call(d3.axisLeft(yScale2));

const xScale2 = d3
.scaleBand()
.range([0, width2])
.domain(sample2.map((s)=> s.x ))
.padding(0.2);

chart2
.append('g')
.attr('stroke', '#FFF')
.attr('class', 'axe')
.attr('transform', `translate(0, ${height2})`)
.call(d3.axisBottom(xScale2));

// TITRE svg DU GRAPHS
svg2
.append('text')
.attr('x', -(height2 / 2) - margin2)
.attr('y', 20)
.attr('transform', 'rotate(-90)')
.attr('text-anchor', 'middle')
.attr('fill', '#FFF')
.text('Emission de CO2 (g)')

svg2
.append('text')
.attr('x', width2 / 2 + margin2)
.attr('y', height2 + margin2*2)
.attr('text-anchor', 'middle')
.attr('fill', '#FFF')
.text('Heures')

var line2 = d3.line()
    .x(function (d) {
      return xScale2(d.x);
    }) // set the x values for the line generator
    .y(function (d) {
      return yScale2(d.y - 90);
    }) // set the y values for the line generator
    .curve(d3.curveMonotoneX) // apply smoothing to the line
  
 // 9. Append the path, bind the data, and call the line generator
 svg2.append("path")
 .datum(sample2) // 10. Binds data to the line
 .attr("class", "line") // Assign a class for styling
 .attr("d", line2) // 11. Calls the line generator
 .attr('transform', `translate(${margin2+17}, 0)`);

  // 12. Appends a circle for each datapoint 
  const dotGroup2 = svg2
  .selectAll(".dot")
  .data(sample2)
  .enter().append("circle") // Uses the enter().append() method
  .attr("class", "dot") // Assign a class for styling
  .attr("cx", function(d) { return xScale2(d.x) })
  .attr("cy", function(d) { return yScale2(d.y - 90) })
  .attr("r", 5)
  .attr('transform', `translate(${margin2+17}, 0)`);

  dotGroup2.on("mouseenter", function(a, b) { 
    const thisRect = d3.select(this);
    thisRect.attr('class', 'dotNotFocus');
    svg2
    .append('text')
    .attr('x', b*40)
    .attr('y', 300)
    .attr('fill', '#FFF')
    .attr('id', 'current')
    .text(a.y)
  })

  dotGroup2.on("mouseout", function() { 
    const thisRect = d3.select(this);
    thisRect.attr('class', 'dot');
    d3.select('#current').remove();
  })
