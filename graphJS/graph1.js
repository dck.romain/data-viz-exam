const margin = 50;
const width = 1000 - 2 * margin;
const height = 600 - 2 * margin;

let array1 = [];

data1.forEach((element) => {

  if (typeof array1[element['fields']['nm_sttn']] === 'undefined') {
    array1[element['fields']['nm_sttn']] = [];
    array1[element['fields']['nm_sttn']]['name'] = element['fields']['nm_sttn'];
  }

  array1[element['fields']['nm_sttn']].push(element['fields']['valeur']);
})

let sample1 = [];

for (const key in array1) {

  let average = 0;

  let div = 0;

  array1[key].forEach((value) => {

    if (typeof value !== 'undefined') {
      average = average + value;
      div++;
    }
  })

  average = average / div;
  array1[key]['average'] = Math.trunc(average);

  sample1.push(array1[key]);
}

const svg1 = d3.select('#svg1');

// AXES1
const chart = svg1
.append('g')
.attr('transform', `translate(${margin}, ${margin})`);

const yScale = d3
.scaleLinear()
.range([height, 0])
.domain([0, 100]);

chart
.append('g')
.attr('stroke', '#FFF')
.attr('class', 'axe')
.call(d3.axisLeft(yScale));

const xScale = d3
.scaleBand()
.range([0, width])
.domain(sample1.map((s)=> s.name))
.padding(0.2);

chart
.append('g')
.attr('stroke', '#FFF')
.attr('class', 'axe')
.attr('transform', `translate(0, ${height})`)
.call(d3.axisBottom(xScale));

// LIGNES HORIZONTAL
chart
.append('g')
.attr('class', 'grid')
.attr('opacity', 0.3)
.call(
  d3.axisLeft()
  .scale(yScale)
  .tickSize(- width, 0, 0)
  .tickFormat(``)
)

// TITRE SVG DU GRAPHS
svg1
.append('text')
.attr('x', -(height / 2) - margin)
.attr('y', margin / 2.4)
.attr('transform', 'rotate(-90)')
.attr('text-anchor', 'middle')
.attr('fill', '#FFF')
.text('Valeur de pollution (ug.m-3)')

svg1
.append('text')
.attr('x', width / 2 + margin)
.attr('y', height + margin + 50)
.attr('text-anchor', 'middle')
.attr('fill', '#FFF')
.text('Nom des stations de la ville de Poitiers')

// GENERATION DES BAR EN GROUPE

const barGroups = chart
.selectAll()
.data(sample1)
.enter()
.append('g')

barGroups
.append('rect')
.attr('class', 'bar')
.transition()
.duration(0)
.delay(function(d,i){ return i * (200 / 4); })
.style('height', (g) => height + yScale(g.average))

.attr('x', (g) => xScale(g.name))
.attr('y', (g) => yScale(g.average))
.attr('height', (g) => height - yScale(g.average))
.attr('width', xScale.bandwidth())
.attr('class', 'color1')

barGroups
.append('text')
.attr('x', (g) => xScale(g.name) + 71)
.attr('y', (g) => yScale(g.average) + 20)
.attr('class', 'color2')
.text((s)=> `${s.average}`)

barGroups.on("mouseenter", function (actual, i) {
  const y = yScale(actual.average);
  const thisRect = d3.select(this);

  thisRect
  // .transition()
  // .duration(300)
  .attr('x', (s) => xScale(s.name) - 5)
  .attr('width', xScale.bandwidth() + 10)

  chart
  .append('line')
  .attr('id', 'limit')
  .attr('x1', 0)
  .attr('y1', y)
  .attr('x2', width)
  .attr('y2', y)
  .attr('stroke', 'red')
})

barGroups.on("mouseleave", function (actual, i) {

  const thisRect = d3.select(this);

  thisRect
  .transition()
  .duration(300)
  .attr('x', (s) => xScale(s.name) - 0)
  .attr('width', xScale.bandwidth() + 0)

  d3.select('#limit').remove();
})
