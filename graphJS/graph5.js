

const data5map = data6.map(x => {
    return {
      gare : x.fields.gc_obo_gare_origine_r_name,
      type: x.fields.gc_obo_type_c
    };
  });

  var json5 = data5map.group(function (item) {
    if(item.gare){
      return item.gare;
    }
  });

  data = [];

  json5.forEach(element => {
      if(element.key !== 'undefined' && element.data.length >70)
      data.push({ Name : element.key , Count : element.data.length})
  
  });
  dataset = {}
  dataset = ({children : data})

        var diameter = 800;
        var color = d3.scaleOrdinal(d3.schemeCategory20);

        var bubble = d3.pack(dataset)
            .size([diameter, diameter])
            .padding(1.5);

        var svg5 = d3.select("#svg5")
            .attr("width", diameter)
            .attr("height", diameter)
            .attr("class", "bubble");

        var nodes = d3.hierarchy(dataset)
            .sum(function(d) { return d.Count; });

        var node = svg5.selectAll(".node")
            .data(bubble(nodes).descendants())
            .enter()
            .filter(function(d){
                return  !d.children
            })
            .append("g")
            .attr("class", "node")
            .attr("transform", function(d) {
                return "translate(" + d.x + "," + d.y + ")";
            });

        node.append("title")
            .text(function(d) {
                return d.Name + ": " + d.Count;
            });

        node.append("circle")
            .attr("r", function(d) {
                return d.r;
            })
            .style("fill","#AFEEEE");

        node.append("text")
            .attr("dy", ".2em")
            .style("text-anchor", "middle")
            .text(function(d) {
                return d.data.Name.substring(0, d.r / 3);
            })
            .attr("font-family", "sans-serif")
            .attr("font-size", function(d){
                return d.r/6;
            })
            .attr("fill", "#1e223d");

        node.append("text")
            .attr("dy", "1.3em")
            .style("text-anchor", "middle")
            .text(function(d) {
                return d.data.Count;
            })
            .attr("font-family",  "Gill Sans", "Gill Sans MT")
            .attr("font-size", function(d){
                return d.r/6;
            })
            .attr("fill", "#1e223d");

        d3.select(self.frameElement)
            .style("height", diameter + "px");
