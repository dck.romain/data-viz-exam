const data6map = data6.map(x => {
  return {
    nature: x.fields.gc_obo_nature_c,
    type: x.fields.gc_obo_type_c
  };
});

var json6 = data6map.group(function(item) {
  if (item.type) {
    return item.type;
  }
});

var objJson6 = {
  name: "OBJECT LOST",
  children: []
};

var j = 0;
for (let i = 0; i < json6.length; i++) {
  const item = json6[i];

  const itemData = json6[i].data;
  let tmpChild = itemData.group(function(item) {
    return item.nature;
  });

  let tmpChildChild = tmpChild;
  let p = 0;
  let total = 0;
  let max = 0;
  let other = 0;
  for (let k = 0; k < tmpChildChild.length; k++) {
    const child = tmpChildChild[k];
    total += child.data.length;
    if (
      child.data.length > max &&
      (child.key.split(/,| /)[0] != "Autre" &&
        child.key.split(/,| /)[0] != "Autres")
    )
      max = child.data.length;
  }
  if (max > 200) {
    objJson6.children[j] = {
      name: item.key.split(/,| |:/)[0],
      children: []
    };
    for (let k = 0; k < tmpChildChild.length; k++) {
      const child = tmpChildChild[k];
      if (
        (child.data.length > (total * 10) / 100 || child.data.length == max) &&
        (child.key.split(/,| /)[0] != "Autre" &&
          child.key.split(/,| /)[0] != "Autres")
      ) {
        let objChild = {
          name: child.key.split(/,| /)[0],
          size: child.data.length
        };
        objJson6.children[j].children[p] = objChild;
        p++;
      } else other += child.data.length;
    }
    let objChild = { name: "Autres", size: other };
    objJson6.children[j].children[p] = objChild;
    j++;
  }
}

//_______________________________________ END MAPPING DATA :)

var nodeData = objJson6;

// Variables
var width6 = 950;
var height6 = 950;
var radius6 = Math.min(width6, height6) / 2;
var color = d3.scaleOrdinal(d3.schemeCategory20b);

var g = d3
  .select("#svg6")
  .attr("width", width6)
  .attr("height", height6)
  .append("g")
  .attr("transform", "translate(" + width6 / 2 + "," + height6 / 2 + ")");

var partition = d3.partition().size([2 * Math.PI, radius6]);

var root = d3.hierarchy(nodeData).sum(function(d) {
  return d.size;
});

partition(root);
var arc = d3
  .arc()
  .startAngle(function(d) {
    return d.x0;
  })
  .endAngle(function(d) {
    return d.x1;
  })
  .innerRadius(function(d) {
    return d.y0;
  })
  .outerRadius(function(d) {
    return d.y1;
  });

const test = g
  .selectAll("g")
  .data(root.descendants())
  .enter()
  .append("g")
  .attr("class", "node")
  .append("path")
  .attr("display", function(d) {
    return d.depth ? null : "none";
  })
  .attr("d", arc)
  .style("stroke", "#2B3473")
  .style("fill", "#AFEEEE");
g.selectAll("g")
  .data(root.descendants())
  .enter()
  .append("g")
  .attr("class", "node")
  .append("path")
  .attr("display", function(d) {
    return d.depth ? null : "none";
  })
  .attr("d", arc)
  .style("stroke", "#2B3473")
  .style("fill", "#AFEEEE");
// .style("fill", function (d) { return color((d.children ? d : d.parent).data.name); });

g.selectAll(".node")
  .append("text")
  .attr("transform", function(d) {
    return "translate(" + arc.centroid(d) + ")";
  })
  .attr("dx", "-20") // radius margin
  .attr("font-size", "10px") // radius margin
  .attr("dy", ".5em") // rotation align
  .text(function(d) {
    return d.parent ? d.data.name : "";
  });

// });

function computeTextRotation(d) {
  var angle = ((d.x0 + d.x1) / Math.PI) * 90;
  return angle < 120 || angle > 270 ? angle : angle + 180;
}

test.on("mouseenter", function(a, b) {
  d3.select(this).style("fill", "#E6F4F5");
});

test.on("mouseout", function() {
  d3.select(this).style("fill", "#AFEEEE");
});
