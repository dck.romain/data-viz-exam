const d3margin = 50;
const d3width = 1000 - 2 * d3margin;
const d3height = 600 - 2 * d3margin;

Object.defineProperty(Array.prototype, 'group', {
    enumerable: false,
    value: function (key) {
      var map = {};
      this.forEach(function (e) {
        var k = key(e);
        map[k] = map[k] || [];
        map[k].push(e);
      });
      return Object.keys(map).map(function (k) {
        return {key: k, data: map[k]};
      });
    }
  });

  
  const data3map = data4.map((x)=>{
    return {
      'date_debut_1' : x.fields.periode_date_debut_1, 
      'date_fin_1' : x.fields.periode_date_fin_1, 
      'date_debut_2' : x.fields.periode_date_debut_2, 
      'date_fin_2' : x.fields.periode_date_fin_2, 
      'date_debut_3' : x.fields.periode_date_debut_3, 
      'date_fin_3' : x.fields.periode_date_fin_3, 
      'intitule_date_1' : x.fields.periode_nom_1, 
      'intitule_date_2' : x.fields.periode_nom_2, 
      'intitule_date_3' : x.fields.periode_nom_3, 
    }
  })
  
  //console.log(data3map)

 
 /* var data3datedebut = data3map.group(function (item) {
            return new Date(item.date_debut)
  });
  
  var data3datefin = data3map.group(function (item) {
        return new Date(item.date_fin)
});*/

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

const d3sample2 = []

for(let i=0;i < 12; i++) {
    d3sample2.push({ mois : monthNames[i] })
}

for(let i = 0 ; i < data3map.length; i++){
    if(data3map[i].intitule_date_1 === "Manifestation"){
       let date1 = new Date(data3map[i].date_debut_1)
       d3sample2.push({ mois : monthNames[date1.getMonth()], annee : date1.getYear() })
       //console.log("Manifestation 1 " + data3map[i].intitule_date_1 +  " date " + date1.getMonth() )
    }
    
    if(data3map[i].intitule_date_2 === "Manifestation"){
       let date2= new Date(data3map[i].date_debut_2)
       d3sample2.push({ mois : monthNames[date2.getMonth()], annee : date2.getYear() })
       //console.log("Manifestation 2 " + data3map[i].intitule_date_2  + " date " + date2.getMonth())
    }
    if(data3map[i].intitule_date_3 === "Manifestation"){
       let date3 = new Date(data3map[i].date_debut_3)
       d3sample2.push({ mois : monthNames[date3.getMonth()], annee : date3.getYear() })
       //console.log("Manifestation 3 "+ data3map[i].intitule_date_3  + " date " + date3.getMonth())
    }
    
}


 var data3datedebut = d3sample2.group(function (item) {
            return item.mois
  });

  const d3sample1 = []
//console.log(data3datedebut)
  for(let i = 0 ; i < data3datedebut.length; i++){
    d3sample1.push({ date : data3datedebut[i].key , value : data3datedebut[i].data.length })

  }

  //console.log(d3sample1)

 
const d3svg = d3.select('#d3svg');
const d3chart = d3svg.append('g')
  .attr('transform', `translate(${d3margin}, ${d3margin})`);


// Scaling Function and draw axis
const d3yScale = d3.scaleLinear()
  .range([d3height, 0])
  .domain([0, 30]);
  
  d3chart
  .append('g')
  .attr('stroke', '#FFF')
  .attr('class', 'axe')
  .call(d3.axisLeft(d3yScale));


const d3xScale = d3.scaleBand()
  .range([0, width])
  .domain(d3sample1.map((s) => s.date))
  .padding(0.2)

  d3chart
.append('g')
.attr('stroke', '#FFF')
.attr('class', 'axe')
.attr('transform', `translate(0, ${d3height})`)
.call(d3.axisBottom(d3xScale));

d3chart
.append('g')
.attr('class', 'grid')
.attr('opacity', 0.3)
.call(
  d3.axisLeft()
  .scale(d3yScale)
  .tickSize(- width, 0, 0)
  .tickFormat(``)
)
 
// LABELS

d3svg
  .append('text')
  .attr('class', 'label')
  .attr('x', -(d3height / 2) - d3margin)
  .attr('y', d3margin / 2.4)
  .attr('transform', 'rotate(-90)')
  .attr('text-anchor', 'middle')
  .attr('fill', '#F6F7F8')
  .text('Nombres de manifestation')
 
d3svg.append('text')
  .attr('class', 'label')
  .attr('x', d3width / 2 + d3margin)
  .attr('y', d3height + d3margin * 1.7)
  .attr('text-anchor', 'middle')
  .attr('fill', '#F6F7F8')
  .text('Mois')

d3svg.append('text')
  .attr('class', 'source')
  .attr('x', d3width - d3margin / 2)
  .attr('y', d3height + d3margin * 1.7)
  .attr('text-anchor', 'start')
  .attr('fill', '#F6F7F8')
  .text('Source 2018')


// Interractivity and chart construction

const d3barGroups = d3chart.selectAll()
  .data(d3sample1)
  .enter()
  .append('g')

d3barGroups
  .append('rect')
  .attr('class', 'bar')
  .attr("fill",'#AFEEEE')
  .attr('x', (g) => d3xScale(g.date))
  .attr('y', (g) => d3yScale(g.value))
  .attr('height', (g) => d3height - d3yScale(g.value))
  .attr('width', d3xScale.bandwidth())
 
d3barGroups.on('mouseenter', function (actual, i) {
  const y = d3yScale(actual.value)
  const x = d3xScale(actual.date);
 
  d3.select(this)
    .transition()
    .duration(300)
    .attr('opacity', 0.6)
    .attr('x', (a) => d3xScale(a.date) - 5)
    .attr('width', d3xScale.bandwidth() + 10)
 
    d3chart.append('text')
    .attr('id', 'info')
    .attr('fill', 'white')
    .attr('opacity', 0.8)
    .attr('font-size', '14px')
    .attr('text-anchor', 'middle')
    .attr('x', x + d3xScale.bandwidth() / 2)
    .attr('y', y + 25)
    .text(`${actual.date} : ${actual.value}`);


 
})

d3barGroups.on("mouseleave", function (actual, i) {
  d3.select(this).attr("opacity", 1)
  d3chart.selectAll('.divergence').remove()
  d3chart.selectAll('#limit').remove()
  d3chart.selectAll('#info').remove()
 
  d3.select(this)
    .transition()
    .duration(300)
    .attr('opacity', 1)
    .attr('x', (a) => d3xScale(a.date))
    .attr('width', d3xScale.bandwidth())
})
  