const mapdata4 = data4.map((x)=>{
  return {
    'commune_etab' : x.fields.commune_etab,
  }
})

var json4 = mapdata4.group(function (item) {
  if(item.commune_etab){
    return item.commune_etab;
  }
});

var json4MapAll = json4.map((x)=>{

  return {
    'label' : x.key, 
    'value' : x.data.length, 
    'color' : '#AFEEEE'
  }

});

const json4Map = json4MapAll.filter(x => (x.value > 50 && x.label != 'undefined'));

//--------------------------END DATA MAPPING

// console.log(json4Map);


var pie = new d3pie("pieChart", {
	"footer": {
		"color": "#E6F4F5",
		"fontSize": 10,
		"font": "open sans",
		"location": "bottom-left"
	},
	"size": {
		"canvasWidth": 650,
		"pieInnerRadius": "51%",
		"pieOuterRadius": "100%"
	},
	"data": {
		"sortOrder": "value-desc",
		"content": [
      ...json4Map
		]
	},
	"labels": {
		"outer": {
			"pieDistance": 32
		},
		"inner": {
			"hideWhenLessThanPercentage": 3
		},
		"mainLabel": {
      "fontSize": 11,
			"color": "#FFF"
		},
		"percentage": {
			"color": "#1e223d",
			"decimalPlaces": 1
		},
		"value": {
			"color": "#1e223d",
			"fontSize": 13
		},
		"lines": {
			"enabled": true
		},
		"truncation": {
			"enabled": true
		}
	},
	"tooltips": {
		"enabled": true,
		"type": "placeholder",
		"string": "{label}: {value} manifestations",
		"styles": {
			"fadeInSpeed": 500,
			"backgroundOpacity": 1
		}
	},
});